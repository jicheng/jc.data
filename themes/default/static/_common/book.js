/******************************************************************************
 * Extended scripts for pages with data-type="book".
 *****************************************************************************/

(function (root, factory) {
  // Browser globals
  root.utils = factory(root.utils);
}(this, function (utils) {

'use strict';

const {ASCII_WHITESPACE} = utils;

/** @public */
class Jicheng extends utils.Jicheng {
  initPanel() {
    super.initPanel();

    const r = String.raw;
    const html = r`
      <details open id="page-panel-tabpanel-option-view">
        <summary>檢視</summary>
        <label for="useAncient" aria-description="設定顯示的版本：
• 今版：呈現貼近現代閱讀習慣的內容，例如按現代習慣分章分段，加入現代標點，將罕用異體字轉為現代常用字等等。
• 古版：呈現貼近古籍原始樣貌的內容，例如不使用現代標點，使用直式排版等。
• 自動：程式將按目前瀏覽的頁面及其他選項自動判斷。">版式: <select id="useAncient" size="1">
          <option value="auto">自動</option>
          <option value="false" selected>今版</option>
          <option value="true">古版</option>
        </select></label>
        <label for="useZhu" aria-description="設定如何呈現注文。注文是注家對原文的注解，例如王冰注《素問》的文字。
• 自動：程式將按目前瀏覽的頁面及其他選項自動判斷。
• 比對：上色顯示注解。
• 接受：顯示注解。
• 拒絕：不顯示注解。">注: <select id="useZhu" size="1">
          <option value="auto" selected>自動</option>
          <option value="diff">比對</option>
          <option value="accept">接受</option>
          <option value="reject">拒絕</option>
        </select></label>
        <label for="useShu" aria-description="設定如何呈現疏文。疏文是第二注家對原文或注文的注解，例如林億、高保衡校注《素問王冰注》的文字。
• 自動：程式將按目前瀏覽的頁面及其他選項自動判斷。
• 比對：上色顯示注解。
• 接受：顯示注解。
• 拒絕：不顯示注解。">疏: <select id="useShu" size="1">
          <option value="auto" selected>自動</option>
          <option value="diff">比對</option>
          <option value="accept">接受</option>
          <option value="reject">拒絕</option>
        </select></label>
        <label for="useJiao" aria-description="設定如何呈現校文。校文是現代整理者加入的注釋或校訂說明。
• 自動：程式將按目前瀏覽的頁面及其他選項自動判斷。
• 比對：上色顯示注解。
• 接受：顯示注解。
• 拒絕：不顯示注解。">校: <select id="useJiao" size="1">
          <option value="auto" selected>自動</option>
          <option value="diff">比對</option>
          <option value="accept">接受</option>
          <option value="reject">拒絕</option>
        </select></label><button id="useAdvancedAnnotationBtn" type="button" class="plain" aria-label="進階篩選" aria-description="進階篩選本書的注疏校文。">⚙️</button>
        <label for="useDing" aria-description="設定如何呈現訂文。訂文是現代整理者對原典所做的修訂，或不同版本的文字。
• 自動：程式將按目前瀏覽的頁面及其他選項自動判斷。
• 比對：上色顯示修訂版增加的文字及刪除的原典文字。
• 接受：顯示修訂版的文字。
• 拒絕：顯示原典的文字。">訂: <select id="useDing" size="1">
          <option value="auto" selected>自動</option>
          <option value="diff">比對</option>
          <option value="accept">接受</option>
          <option value="reject">拒絕</option>
        </select></label><button id="useAdvancedVersionBtn" type="button" class="plain" aria-label="進階篩選" aria-description="進階篩選本書的訂文。">⚙️</button>
      </details>`;

    document.getElementById('page-panel-tabpanel-option').insertAdjacentHTML('afterbegin', html);

    {
      const elem = document.getElementById('useAdvancedAnnotationBtn');

      const ANNOTATION_SELECTOR = [
        '[data-rev="注"][data-ver]',
        '[data-rev="疏"][data-ver]',
        '[data-rev="校"][data-ver]',
      ].join(',');

      const GET_DEFAULT_FILTER = () => {
        const rv = {
          '注': {'': 2},
          '疏': {'': 2},
          '校': {'': 2},
        };
        for (const elem of document.querySelectorAll(ANNOTATION_SELECTOR)) {
          const rev = elem.getAttribute('data-rev');
          const ver = elem.getAttribute('data-ver');
          rv[rev][ver] = 2;
        }
        return rv;
      };

      elem.addEventListener('click', (event) => {
        const title = `進階篩選本書不同來源的注疏校文（JSON，或清空重設）：
• 索引鍵："": 不明或未定義版本的注釋
• 鍵值：0: 拒絕；1: 接受；其他值: 插入
• 鍵值可用 {"value": <狀態值>, "css": {"<屬性>": "<屬性值>", ...}} 的格式自訂樣式`;
        let value = prompt(title, JSON.stringify(this.options.advancedAnnotationFilter || GET_DEFAULT_FILTER()));
        while (true) {
          if (value === null) {
            return;  // cancel
          }
          if (value === '') {
            this.options.advancedAnnotationFilter = null;  // reset
            return;
          }
          try {
            const data = JSON.parse(value);
            if (!(typeof data === 'object' && data !== null && !Array.isArray(data))) {
              throw new Error(`輸入資料須為一物件。`);
            }
            for (const key of ['注', '疏', '校']) {
              if (!(typeof data[key] === 'object' && data[key] !== null && !Array.isArray(data[key]))) {
                throw new Error(`「${key}」的值須為一物件`);
              }
              for (const ver in data[key]) {
                if (ASCII_WHITESPACE.test(ver)) {
                  throw new Error(`「${key}」的鍵值「${ver}」錯誤：含有空白字元`);
                }
              }
            }
            this.options.advancedAnnotationFilter = data;
            break;
          } catch (ex) {
            alert(`物件格式錯誤：${ex.message}`);
            value = prompt(title, value);
          }
        }
      });
    }

    {
      const elem = document.getElementById('useAdvancedVersionBtn');

      const ANNOTATION_SELECTOR = '[data-rev="訂"][data-ver]';

      const GET_DEFAULT_FILTER = () => {
        const rv = {'': 0};
        for (const elem of document.querySelectorAll(ANNOTATION_SELECTOR)) {
          const rev = elem.getAttribute('data-rev');
          const ver = elem.getAttribute('data-ver');
          rv[ver] = 0;
        }
        return rv;
      };

      elem.addEventListener('click', (event) => {
        const title = `進階篩選本書不同來源的訂文（JSON，或清空重設）：
• 索引鍵："": 笈成修訂版的文字；"*": 原典的文字
• 鍵值：0: 拒絕；1: 接受；2: 刪除；其他值: 插入
• 鍵值可用 {"value": <狀態值>, "css": {"<屬性>": "<屬性值>", ...}} 的格式自訂樣式`;
        let value = prompt(title, JSON.stringify(this.options.advancedVersionFilter || GET_DEFAULT_FILTER()));
        while (true) {
          if (value === null) {
            return;  // cancel
          }
          if (value === '') {
            this.options.advancedVersionFilter = null;  // reset
            return;
          }
          try {
            const data = JSON.parse(value);
            if (!(typeof data === 'object' && data !== null && !Array.isArray(data))) {
              throw new Error(`輸入資料須為一物件。`);
            }
            for (const ver in data) {
              if (ASCII_WHITESPACE.test(ver)) {
                throw new Error(`「${key}」的鍵值「${ver}」錯誤：含有空白字元`);
              }
            }
            this.options.advancedVersionFilter = data;
            break;
          } catch (ex) {
            alert(`物件格式錯誤：${ex.message}`);
            value = prompt(title, value);
          }
        }
      });
    }
  }
}

return Object.assign(utils, {
  Jicheng,
});

}));
